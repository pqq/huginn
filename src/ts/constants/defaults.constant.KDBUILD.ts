// 18.12.28 // frode burdal klevstul // www.thisworld.is

export const version_details = {
      number: "2"
    , name:   "bragi"
}

export const muninn_url: string = 'http://127.0.0.1/muninn/src/muninn.py';              // @ENV_DEV @NLS
// typescript will not process comments, so environment-specific data has to be written in pure javascript
//@ export const muninn_url = 'http://127.0.0.1/midgard/environments/beta/api/muninn.py';   // @ENV_BETA @NLS
//@ export const muninn_url = '/api/muninn.py';                       // @ENV_PROD @NLS
