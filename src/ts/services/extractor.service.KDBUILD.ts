// 19.01.01 // frode burdal klevstul // www.thisworld.is

export class ExtractorService {

    constructor() {}

    public getParameterValueFromRequestUrl(url: string, parameter: string){
        let value: string = "";
        if (url.includes(parameter + '=')){
            value = url.split(parameter + '=')[1];
            value = value.split('&')[0];
        }
        return value;
    }

}
