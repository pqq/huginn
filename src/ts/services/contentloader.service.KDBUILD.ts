// 18.12.28 // frode burdal klevstul // www.thisworld.is

import {muninn_url}         from '../constants/defaults.constant.KDBUILD.js';
import {WwwService}         from '../services/www.service.KDBUILD.js'
import {FscService}         from '../services/fsc.service.KDBUILD.js';
import {LayoutService}      from '../services/layout.service.KDBUILD.js';
import {HuginnCfg}          from '../classes/huginncfg.class.KDBUILD.js';
import {ExtractorService}   from '../services/extractor.service.KDBUILD.js';

export class ContentLoaderService {

    constructor() {}

    public loadData(huginn_cfg: HuginnCfg, request_parameters: string, callback: Function){
        // we have to declare "stuff" inside 'loadData()' so it will be
        // in scope for the callback function in 'getWebResource()'

        let wwwService: WwwService = new WwwService;
        let fscService: FscService = new FscService;
        let layoutService: LayoutService = new LayoutService;
        let extractorService: ExtractorService = new ExtractorService;

        function parseMuninnLinks(input: string){
            // original code, which causes problems using kDeployer minification because of `//`
            // in the code which is treated as a comment:
            // input = input.replace(/muninn\:\/\//g, '?/');
            // (see: https://gitlab.com/klevstul/kDeployer/issues/16):
            // new, kDeployer-safe code:
            input = input.replace(new RegExp('muninn:\/\/', 'g'), '?/');
            return input;
        }

        function parseExternalLinks(input: string){
            let re: RegExp = /<a (.*?)">e:(.*?)<\/a>/ig;
            input = input.replace(re, '<a $1" target="_blank">$2^</a>');
            return input;
        }

        function parseStandaloneLinks(input: string){
            let re: RegExp = /<a (.*?)">s:(.*?)<\/a>/ig;
            input = input.replace(re, '<a $1&standalone=true" target="_blank">$2^</a>');
            return input;
        }

        function parseCdnLinks(input: string){
            let re: RegExp = /<a href="(https:\/\/gitlab.com)?(.*?)">c:(.*?)<\/a>/ig;
            input = input.replace(re, '<a href="' + huginn_cfg.miscellaneous.cdn.replace('[path]', '$2') + '" target="_blank">$3^</a>');
            return input;
        }

        function loadSingleEntry(request_parameters: string, callback: Function){
            wwwService.getWebResource(muninn_url + '?' + request_parameters, function(response: string) {
                let response_json = JSON.parse(response);
                let html = response_json.entries[0].entry_content_html;
                let fsc = response_json.entries[0].feed_special_code;

                document.title = huginn_cfg.layout.title + ' ^ ' + response_json.entries[0].entry_title;
                html = parseMuninnLinks(html);
                html = parseExternalLinks(html);
                html = parseStandaloneLinks(html);
                html = parseCdnLinks(html);
                html = fscService.fscProcess(html, fsc);

                let element = document.getElementById("h_main");
                if (element) {
                    element.innerHTML = '<div style="overflow-x:auto;"><div class="row"><div class="col-1 hideable">&nbsp;</div><div class="col-10 content">'+ html + '</div><div class="col-1 hideable">&nbsp;</div></div></div>';
                }
                callback();
            });
        }

        function loadRawWebRequest(request_parameters: string){
            let rawMuninnRequestUrl = muninn_url + '?' + request_parameters;
            window.location.href = rawMuninnRequestUrl;
        }

        function loadListOfEntries(request_parameters: string, callback: Function){
            wwwService.getWebResource(muninn_url + '?' + request_parameters, function(response: string) {
                let response_json = JSON.parse(response);

                document.title = huginn_cfg.layout.title + ' ^ ' + extractorService.getParameterValueFromRequestUrl(request_parameters, 'bundle_id');

                let entries = response_json.entries;
                for (let entry in entries) {
                    let element = document.getElementById("h_main");

                    if (element) {
                        let thumbnail = layoutService.getFirstImage(entries[entry].entry_content_html);
                        if (thumbnail !== ''){
                            thumbnail = '<br><img class="thumbnail" src="'+thumbnail+'">';
                        }

                        let ingress_length: number = 200;
                        let ingress = entries[entry].entry_content_text.substring(0,ingress_length);
                        if (ingress !== ''){
                            if (entries[entry].entry_content_text.length > ingress_length){
                                ingress = ingress + '...';
                            }
                            ingress = '<div class="ingress">' +  ingress + '</div>';
                        }

                        let timestamp_profile = entries[entry].entry_published_date_display.split(' ')[0];
                        if (timestamp_profile !== ''){
                            timestamp_profile += ' - ' + entries[entry].feed_profile;
                            timestamp_profile = '<div class="timestamp">' + timestamp_profile + '</div>';
                        }

                        let target = '_top';
                        let external_link_symbol = '';
                        if (entries[entry].entry_link.startsWith("http")){
                            target = '_blank';
                            external_link_symbol = '^';
                        }

                        let newEl = document.createElement('div');
                        newEl.className = "row";

                        newEl.innerHTML = `
                            <div class="col-1 hideable">&nbsp;</div>
                            <div class="col-10">
                                <a href="` + parseMuninnLinks(entries[entry].entry_link) + `" target="` + target + `">` + entries[entry].entry_title + external_link_symbol + `</a>
                                ` +  thumbnail + `
                                ` +  ingress + `
                                ` +  timestamp_profile + `
                            </div>
                            <div class="col-1 hideable">&nbsp;</div>
                        `;
                        element.appendChild(newEl);
                    }
                }
                callback();
            });
        }

        function loadIndex(huginn_cfg: HuginnCfg, callback: Function){
            wwwService.getWebResource(muninn_url + '?' + huginn_cfg.rpc.index, function(response: string) {
                let response_json = JSON.parse(response);
                let html = response_json.entries[0].entry_content_html;

                document.title = huginn_cfg.layout.title;
                html = parseMuninnLinks(html);
                html = parseExternalLinks(html);
                html = parseStandaloneLinks(html);
                html = parseCdnLinks(html);

                let element = document.getElementById('h_main');
                if (element) {
                    element.innerHTML = '<div class="row"><div class="col-1 hideable">&nbsp;</div><div class="col-10 content">'+ html + '</div><div class="col-1 hideable">&nbsp;</div></div>';
                }
                callback();
            });
        }

        if (request_parameters.includes('.md') || request_parameters.includes('.markdown') || request_parameters.includes(".txt") || request_parameters.includes(".html")){
            loadSingleEntry(request_parameters, callback);

        } else if (request_parameters.includes('&file_id')){
            loadRawWebRequest(request_parameters);

        } else if (request_parameters.includes('&')){
            if (!request_parameters.includes('cache=')){
                request_parameters += '&cache=' + huginn_cfg.rpc.cache
            }
            loadListOfEntries(request_parameters, callback);

        } else {
            loadIndex(huginn_cfg, callback);
        }

    }

}
