// 18.12.28 // frode burdal klevstul // www.thisworld.is

import {HuginnCfg}              from '../classes/huginncfg.class.KDBUILD.js';
import {version_details}        from '../constants/defaults.constant.KDBUILD.js';
import {WwwService}             from '../services/www.service.KDBUILD.js'
import {ContentLoaderService}   from '../services/contentloader.service.KDBUILD.js';

export class LayoutService {

    constructor() {}

    public header(huginn_cfg: HuginnCfg){
        let header = huginn_cfg.layout.header;
        header = header.replace('[version]', version_details.number + ' - ' +version_details.name)
        let element = document.getElementById("h_header");
        if (element) {
            element.setAttribute("target", "_top")
            element.innerHTML = header;
            element.style.display = "block";
        }
        let link = document.getElementById("h_header_link");
        if (link) {
            link.setAttribute("href", "?/service_id=" + huginn_cfg.rpc.service_id);
        }
    }

    public footer(huginn_cfg: HuginnCfg){
        let footer = huginn_cfg.layout.footer;
        let huginn_version_string = 'v' + version_details.number;
        let muninn_version_string = 'v' + huginn_cfg.muninn_version.number;
        footer = footer.replace('[version]', huginn_version_string + muninn_version_string);
        footer = footer.replace('[hom]', '<a href="https://hom.is/" target="_blank">hom</a>');
        footer = footer.replace('[timestamp]', '@DATESTAMP_SPLIT');
        let element = document.getElementById("h_footer");
        if (element) {
            element.innerHTML = footer;
            element.style.display = "block";
        }
    }

    public analytics(huginn_cfg: HuginnCfg){
        if (huginn_cfg.analytics.enabled === "true"){
            // add the script to the document for it to be executed
            let script = document.createElement('script');
            script.src = huginn_cfg.analytics.javascript;
            // create the <noscript> tag, containing an image
            let noscript = document.createElement('noscript');
            let img = document.createElement('img');
            img.src = huginn_cfg.analytics.noscript_image;
            noscript.append(img);
            // append the <script> and <noscript> tags to the document head
            let head = document.getElementsByTagName("head")[0];
            head.appendChild(script);
            head.appendChild(noscript);
        }
    }

    public comments(huginn_cfg: HuginnCfg, request_parameters: string){
        // special values in request_include / request_exclude:
        //      '*'          - wildcard
        //      '[index]'    - index page
        if (huginn_cfg.commenting.enabled === "true"){
            let wwwService: WwwService = new WwwService;
            let attribute: string;
            let req_inc_exc: string;
            let include_found:boolean = false;
            let index_page: boolean = false;

            request_parameters = wwwService.urlDecode(request_parameters);

            // check if we are on the index page
            if (!request_parameters.includes('&')){
                index_page = true;
            }

            // include...
            for (req_inc_exc in huginn_cfg.commenting.request_include){
                // skip blanks
                if (huginn_cfg.commenting.request_include[req_inc_exc] === ''){
                    continue;
                }
                // index page
                if (huginn_cfg.commenting.request_include[req_inc_exc] === '[index]'){
                    if (index_page){
                        include_found = true;
                        break;
                    }
                }
                // wildcard
                if (huginn_cfg.commenting.request_include[req_inc_exc] === '*'){
                    include_found = true;
                    break;
                }
                // all other pages
                if (request_parameters.includes(huginn_cfg.commenting.request_include[req_inc_exc])){
                    include_found = true;
                    break;
                }
            }

            // jump out of the function if we have no include match
            if (!include_found){
                return;
            }

            // exclude...
            // jump out of the function if a string in the request_exclude array is found in the request parameters
            for (req_inc_exc in huginn_cfg.commenting.request_exclude){
                // skip blanks
                if (huginn_cfg.commenting.request_exclude[req_inc_exc] === ''){
                    continue;
                }
                // index page
                if (huginn_cfg.commenting.request_exclude[req_inc_exc] === '[index]'){
                    if (index_page){
                        return;
                    }
                }
                // wildcard
                if (huginn_cfg.commenting.request_exclude[req_inc_exc] === '*'){
                    return;
                }
                // all other pages
                if (request_parameters.includes(huginn_cfg.commenting.request_exclude[req_inc_exc])){
                    return;
                }
            }

            let element = document.getElementById("h_comments");
            if (element) {
                // create a div for the comments
                let comment_div = document.createElement("div");

                // change the id to the id defined by the commenting system
                if (huginn_cfg.commenting.div_id) {
                    comment_div.id = huginn_cfg.commenting.div_id;
                }

                // set the css class
                let css_class = "col-10 comments";
                if (huginn_cfg.commenting.div_class) {
                    css_class += " " + huginn_cfg.commenting.div_class;
                }
                comment_div.className = css_class;

                // apply div attributes
                for (attribute in huginn_cfg.commenting.div_attributes){
                    let att = huginn_cfg.commenting.div_attributes[attribute].split("=");
                    comment_div.setAttribute(att[0], att[1]);
                }

                // create divs for comments
                let div1 = document.createElement("div");
                div1.setAttribute("style", "overflow-x:auto;");
                let div2 = document.createElement("div");
                div2.className = "row";
                let div3_1 = document.createElement("div");
                div3_1.className = "col-1 hideable";
                let div3_2 = document.createElement("div");
                div3_2.className = "col-10";
                let div3_3 = document.createElement("div");
                div3_3.className = "col-1 hideable";

                // append divs to the original comment div element
                element.appendChild(div1);
                div1.appendChild(div2);
                div2.appendChild(div3_1);
                div2.appendChild(div3_2);
                div2.appendChild(div3_3);
                div3_2.appendChild(comment_div);

                // add the script to the document for it to be executed
                let script = document.createElement('script');
                script.src = huginn_cfg.commenting.javascript;

                // apply script attributes
                for (attribute in huginn_cfg.commenting.script_attributes){
                    let att = huginn_cfg.commenting.script_attributes[attribute].split("=");
                    script.setAttribute(att[0], att[1]);
                }

                // append the script to the page
                let head = document.getElementsByTagName("head")[0];
                head.appendChild(script);
            }
        }
    }

    private setCss(huginn_cfg: HuginnCfg){
        let wwwService: WwwService = new WwwService;

        function loadCssFromFile(){
            wwwService.getWebResource(huginn_cfg.layout.css_file_to_load, function(response: string) {
                let response_css = response;
                let style = document.createElement("style");
                style.innerText = response_css;
                document.getElementsByTagName("head")[0].appendChild(style);
            });
        }

        function setCssFromCfg(){
            let style = document.createElement("style");
            style.innerText = huginn_cfg.layout.css_content;
            document.getElementsByTagName("head")[0].appendChild(style);
        }

        function setCssLink(){
            let link = document.createElement("link");
            link.href = huginn_cfg.layout.css_link;
            link.type = "text/css";
            link.rel = "stylesheet";
            link.media = "screen,print";
            document.getElementsByTagName("head")[0].appendChild(link);
        }

        if (huginn_cfg.layout.css_content) {
            setCssFromCfg();
        }
        if (huginn_cfg.layout.css_file_to_load) {
            loadCssFromFile();
        }
        if (huginn_cfg.layout.css_link) {
            setCssLink();
        }
    }

    private setMeta(huginn_cfg: HuginnCfg){
        let author = document.createElement('meta');
        author.name = "author";
        author.content = huginn_cfg.meta.author;
        document.getElementsByTagName('head')[0].appendChild(author);

        let description = document.createElement('meta');
        description.name = "description";
        description.content = huginn_cfg.meta.description;
        document.getElementsByTagName('head')[0].appendChild(description);

        let keywords = document.createElement('meta');
        keywords.name = "keywords";
        keywords.content = huginn_cfg.meta.keywords;
        document.getElementsByTagName('head')[0].appendChild(keywords);
    }

    public setUpPage(huginn_cfg: HuginnCfg, request_parameters: string){
        let contentLoaderService: ContentLoaderService = new ContentLoaderService;

        document.title = huginn_cfg.layout.title;

        // for standalone links (s:link_title) we won't format the page
        if (!request_parameters.includes('standalone=true')) {
            this.setCss(huginn_cfg);
            this.setMeta(huginn_cfg);
        }

        // the callback function in loadData() is used for preventing the rendering of the header
        // and footer until the content is presented
        contentLoaderService.loadData(huginn_cfg, request_parameters, function(){
            let layoutService: LayoutService = new LayoutService;
            layoutService.header(huginn_cfg);
            layoutService.analytics(huginn_cfg);
            layoutService.comments(huginn_cfg, request_parameters);
            layoutService.footer(huginn_cfg);
        });
    }

    public getFirstImage(html: string) {
        // example image tag:
        //   <img alt=\"GOPR9503_1450461256625_high.JPG\" src=\"http://1.bp.blogspot.com/-ZG8_M2DKVkQ/VnXJxT2vsSI/AAAAAAAA4wU/uxBYtiigRSQ/s1000/GOPR9503_1450461256625_high.JPG\" />
        // below we do:
        //  remove "s72-c/" from the image urls - fix for blogger's reduced thumbnail sizes
        //  (https://github.com/klevstul/foreNeno/issues/393)

        // note: the following code line is messing up kDeployer's minification regexp
        //       - no comments can be below that line as they won't be removed
        let re      = /<img[^>]+src=\\?"?([^"\s]+)\\?"?[^>]*\/>/g;
        let results = re.exec(html);
        let img     = "";
        if (results) img = results[1];
        img = img.replace('s72-c/','');
        return img;
    }

}
