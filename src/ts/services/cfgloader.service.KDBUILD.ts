// 18.12.28 // frode burdal klevstul // www.thisworld.is

import {WwwService}  from '../services/www.service.KDBUILD.js'
import {HuginnCfg}   from '../classes/huginncfg.class.KDBUILD.js';
import {muninn_url}  from '../constants/defaults.constant.KDBUILD.js';


export class CfgLoaderService {

    constructor() {}

    // load huginn config from muninn
    public loadHuginnConfig(request_parameters: string, callback: Function){
        let wwwService: WwwService = new WwwService;

        // in case a user opens a page with a bundle_id, we have to ignore that bundle_id to set the config's bundle_id
        let request_parameters_call_huginn: string = request_parameters.replace("bundle_id=", "ignore_bundle_id=") + "&bundle_id=huginn";

        wwwService.getWebResource(muninn_url + '?' + request_parameters_call_huginn,  function(response: string) {
            let response_json = JSON.parse(response);
            let huginn_cfg: HuginnCfg = response_json.huginn;
            // muninn's version details are not stored in the huginn object, hence, a small hack to store it in the same object.
            huginn_cfg.muninn_version = response_json.version_details;

            if (!huginn_cfg){
                throw new Error('unable to get huginn config from muninn');
            } else if (!huginn_cfg.rpc.index){
                throw new Error('unable to locate index in huginn_cfg.rpc');
            }

            callback(huginn_cfg);
        });
    }

}
