// 18.12.28 // frode burdal klevstul // www.thisworld.is

export class FscService {

    constructor() {}

    public fscProcess(html: string, fsc: string){

        // asc - allow styled content
        if ( !fsc.match('asc=1') ) {
            html = html.replace(/ style=/gi, 'style_removed=');
        }

        return html;
    }

}
