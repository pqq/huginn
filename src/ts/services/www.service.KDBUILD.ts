// 18.12.28 // frode burdal klevstul // www.thisworld.is

export class WwwService {

    constructor() {}

    public getWebResource(url: string, callback: Function){
        // asyncronous load web resource

        function handleErrorResponse(responseText: string){
            let responseJson = JSON.parse(responseText);
            let newEl = document.createElement('div');
            newEl.className = "error";
            let output = 'DVALINN OC DVERGOM FYR<br><br>';
            if (responseJson.error.code || responseJson.error.message) {
                output += responseJson.error.code + ' ' + responseJson.error.message;
            } else if (responseJson.error) {
                output += ' ' + responseJson.error;
            }

            // note that we can't use the url that is submitted to this function as that is the
            // url to the api. hence, we get the location of the current window.
            let app_url = window.location.href;
            if (app_url.includes('service_id')) {
                let url_object = new URL(app_url);
                let service_id = url_object.searchParams.get('service_id');
                let new_url;
                // in these cases:
                // http://127.0.0.1/huginn/src/app/?/method=list_entries_json&service_id=atestservice
                if (service_id) {
                    new_url = app_url.split('\?')[0] + '?/service_id=' + service_id;
                // in these cases:
                // http://127.0.0.1/huginn/src/app/?/service_id=atestservice&method=list_entries_json
                } else {
                    new_url = app_url.split('&')[0];
                }
                output += '<br><br><a href="'+new_url+'">click here to go home</a>';
            }

            newEl.innerHTML = output;
            document.body.appendChild(newEl);
        }

        let xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function(){
            if(xhr.readyState === 4 && xhr.status === 200){
                if (xhr.responseText.match("\"error\":") ){
                    handleErrorResponse(xhr.responseText);
                } else {
                    callback(xhr.responseText);
                }
            }
        }

        xhr.open('GET', url, true);
        xhr.send();
    }

    public getRequestParameters(){
        // get huginn input data string from the (search part of) the url
        let search_string: string = window.location.search;
        if (!search_string.includes("service_id=")){
            throw new Error('service_id missing from url');
        }
        // remove the "?/" beginning of the search string
        search_string = search_string.split('?/')[1];
        return search_string;
    }

    public urlEncode(url: string){
        return url.replace(/\\/g, '%2F').replace(/ /g, '%20');
    }

    public urlDecode(url: string){
        return url.replace(/%2F/g, '/').replace(/%20/g, ' ');
    }

}
