# huginn

![](https://gitlab.com/klevstul/muninn/raw/master/additional_resources/graphics/huginn.png)  

the **hom** (huginn ok muninn) *notCMS* (not one traditional cms) consist of the front-end huginn (thought) and the back-end muninn (mind).

- for more information visit the [hom wiki](https://gitlab.com/klevstul/muninn/wikis/home)  
- to view **hom** in action visit [hom.is](https://hom.is)  
