### reload browser on file change

using [entr](http://entrproject.org)

required sw:
- entr (install via pacui)
- [reload_browser](http://eradman.com/entrproject/scripts/reload-browser) (download)
- xdotool-git (preinstalled with manjaro i3 - used in "reload_browser" - https://aur.archlinux.org/packages/xdotool-git/)

usage:
`$ ./runme.sh`
